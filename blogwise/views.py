from flask import Blueprint, jsonify, request, render_template, current_app, redirect, url_for,session
from blogwise.models import Article

bp = Blueprint('home', __name__)
@bp.get('/')
def index():
    session['user'] = None
    if session.get('user'):
        articles = Article.filter_by(isFeatured = True)
        return render_template('index.html', a = articles)
    return render_template('index.html')

@bp.get('/api/v1/articles')
def all_articles():
    articles = Article.get_all()
    return jsonify([article.to_json() for article in articles]), 200


@bp.get('/api/v1/articles/<int:article_id>')
def get_api_article(article_id: int):
    article = Article.get_by_id(article_id)
    return jsonify(article.to_json()), 200

@bp.get('/articles')
def get_all_articles():
    article = Article.get_all()
    return render_template("/articles/index.html", article = article)

@bp.post('/api/v1/articles')
def create_article():
    data = request.json
    article = Article.create(**data)
    return jsonify(article.to_json()), 201


@bp.route('/api/v1/articles/<int:article_id>', methods=['POST', 'PATCH'])
def update_article(article_id: int):
    updated_article = Article.update(article_id, request.json.copy())
    return jsonify(updated_article.to_json()), 200


@bp.route('/api/v1/articles/<int:article_id>', methods=['DELETE'])
def delete_article(article_id: int):
    Article.delete(article_id)
    empty = Article.get_by_id(article_id)  # example 
    return jsonify(empty), 204

@bp.route('/articles/<int:article_id>')
def get_article(article_id: int):
    article = Article.get_by_id(article_id)
    return render_template("/articles/article.html", article = article)

@bp.route('/articles/new')
def new_article():
    
    return render_template("/articles/new.html")

@bp.post('/articles')
def post_article():
    article = Article.create(**request.form)
    return redirect(url_for("home.get_article", article_id = article.row_id))

#For editing
@bp.post('/articles/<int:id>/create')
def changed_article(id: int):
    article = Article.filter_by(id)
    return redirect(url_for("home.get_article", article_id = article.row_id))

#Get a edit.
@bp.route('articles/<int:id>/edit', method = ['POST', 'PATCH'])
def get_edit():
    #How do i edit by id, specifically where do i put the id?
     article = Article.update(**request.form)
     return render_template("/articles/edit.html", article = article)